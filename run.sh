#!/bin/bash
STRINIT="Run debug or release version (or neither) [d/r/n]?"
STRSTRT="Starting: "
STRDBG="Debug version of the application"
STRREL="Release version of the application"
STREXE="txtadv-c.exe"
VALID=0
if [[ "$1" = "" ]]; then
	echo $STRINIT
	while [ $VALID = 0 ]; do
		read INPUT
		if [ "$INPUT" = "d" ]; then
			cd bin/Debug
			./$STREXE $2 $3 $4 $5 $6
			VALID=1
		elif [ "$INPUT" = "r" ]; then
			cd bin/Release
			./$STREXE $2 $3 $4 $5 $6
			VALID=1
		elif [ "$INPUT" = "n" ]; then
			sleep 0
		else
			echo Invalid input
			echo $STRINIT
		fi
	done

	elif [[ "$1" = "d" ]] || [[ "$1" = "r" ]] || [[ "$1" = "n" ]]; then
		if [ "$1" = "d" ]; then
			echo $STRSTRT $STRDBG	
			sleep 1
			cd bin/Debug
			./$STREXE $2 $3 $4 $5 $6
		elif [ "$1" = "r" ]; then
			echo $STRSTRT $STRREL
			sleep 1
			cd bin/Release
			./$STREXE $2 $3 $4 $5 $6
		elif [ "$1" = "n" ]; then
			sleep 0
		fi
fi